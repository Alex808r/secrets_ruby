# frozen_string_literal: true

module SimpleRPG
  class InventoryOverflow < StandardError
  end
end
