# frozen_string_literal: true

require 'date'

module SimpleRPG
  class Item
    include Comparable

    attr_reader :name, :value, :found_at

    def initialize(name, value, found_at)
      @name = name
      @value = value
      @found_at = Date.parse(found_at)
    end

    def <=>(other)
      value <=> other.value
    end

    def to_s
      "Name: #{name}\nValue: #{value}\nFound at: #{found_at}"
    end
  end
end
