# frozen_string_literal: true

module Request
  def get(path, params); end

  def post(path, params); end

  def put(path, params); end
end
