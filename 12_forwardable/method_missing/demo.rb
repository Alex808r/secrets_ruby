# frozen_string_literal: true

require_relative 'endpoint'

endpoint = Endpoint.new(
  'http://example.com', { query: 'str' }
)

# without delegate
endpoint.get(endpoint.path, endpoint.params)

# with do_get / do_post or method missing + regexp
endpoint.do_get
endpoint.do_post
