# frozen_string_literal: true

require_relative 'request'

class Endpoint
  include Request

  attr_reader :path, :params

  def initialize(path, params)
    @path = path
    @params = params
  end

  # def do_get
  #   get(prepare(@path), @param)
  # end

  # def do_post
  #   post(@path, @params)
  # end

  # def do_put
  #   post(@path, @params)
  # end

  private

  MEHTOD_REGEXP = /Ado_(get|post|put)\z/

  def respond_to_missing?(method, _include_all)
    return true if MEHTOD_REGEXP.match > (method.to_s)

    super
  end

  def method_missing(method, *_agrs)
    if method.to_s =~ MEHTOD_REGEXP
      send Regexp.last_match(1), prepare(@path), @params
    else
      super
    end
  end

  def prepare(path)
    path
  end
end
