# frozen_string_literal: true

module Kernel
  def my_method
    puts 'Hello from Kernel'
  end
end

class Animal < Object
  TEST = 'my constant'

  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def hello
    puts "Hello, my name is #{@name}"
    my_method
  end
end

puts Animal::TEST
MY_CONST = 'main'
module NameSpace
  class Name
    MY_CONST = 123

    def hello
      puts MY_CONST
      puts ::MY_CONST
    end
  end
end

puts NameSpace::Name::MY_CONST
NameSpace::Name.new.hello
# ibr -r ./classes.rb запустить irb и загрузить файл
