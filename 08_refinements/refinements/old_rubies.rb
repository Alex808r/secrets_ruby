# frozen_string_literal: true

module Refinery
  unless ''.respond_to?(:delete_suffix)
    refine String do
      def delete_suffix(suffix)
        if end_with?(suffix)
          self[0...-suffix.size]
        else
          self
        end
      end
    end
  end
end

using Refinery

result = 'test'.delete_suffix('t')
puts result
