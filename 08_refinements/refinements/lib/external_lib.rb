# frozen_string_literal: true

require_relative 'utils'

module ExternalLib
  class Main
    using ExternalLib::Utils

    def initialize(str)
      @str = str
    end

    def do_work
      puts 'ExtrenalLib doing work...'
      @str.snakecase
    end
  end
end
