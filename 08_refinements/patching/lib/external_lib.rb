# frozen_string_literal: true

require_relative 'string'

module ExternalLib
  class Main
    def initialize(str)
      @str = str
    end

    def do_work
      puts 'ExtrenalLib doing work...'
      @str.snakecase
    end
  end
end
