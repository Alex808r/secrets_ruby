# frozen_string_literal: true

# not avtivated here

using RefinementsModule

# avtivated here
class Foo
  # avtivated here
  def bar
    # avtivated here
  end
  # avtivated here
end
# avtivated here

# =========================

# not avtivated here
class Foo
  # not avtivated here
  def bar
    # not avtivated here
  end
  using RefinementsModule
  # avtivated here
end
# not avtivated here

# =========================
