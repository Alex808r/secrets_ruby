# frozen_string_literal: true

class SuperHash < Hash
  def method_missing(method, *args, &block)
    return store_attribute(method, *args) if method.end_with?('=')

    return self[method] if key?(method)

    super
  end

  def respond_to_missing?(method, include_private = false)
    key?(method) || method.end_with?('=') || super
  end

  private

  def store_attribute(method, *args)
    key = method.to_s.delete_suffix('=')
    self[key.to_sym] = args[0]
  end
end

s = SuperHash.new
s.name = 'Joe'
puts s.name
puts s.respond_to?(:name)
puts s.respond_to?(:name=)

# super_hash.rb:21:in `key': wrong number of arguments (given 0, expected 1) (ArgumentError)
# 	from super_hash.rb:21:in `<main>'
# s.key = 'hi'
# puts s.key
