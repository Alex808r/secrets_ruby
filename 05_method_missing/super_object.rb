# frozen_string_literal: true

class SuperObject < BasicObject
  def initialize
    @proxy = {}
  end

  def method_missing(method, *args, &block)
    return store_attribute(method, *args) if method.end_with?('=')

    return __proxy__[method] if __proxy__.key?(method)

    super
  end

  def respond_to_missing?(method, include_private = false)
    key?(method) || method.end_with?('=') || super
  end

  def inspect
    __proxy__.inject("#<SuperHash: #{__id__.to_s(16)}") do |str, el|
      str + " @#{el[0]}=#{el[1]} "
    end.strip + '>'
  end

  def respond_to?(method)
    __proxy__.key?(method) || method.end_with?('=') || super
  end

  private

  def store_attribute(method, *args)
    key = method.to_s.delete_suffix('=')
    __proxy__[key.to_sym] = args[0]
  end

  def __proxy__
    @proxy
  end
end

s = SuperObject.new
s.name = 'Joe'
puts s.name

s.key = 'Doe'
puts s.key

puts s.inspect
puts s.respond_to?(:name)
puts s.respond_to?(:name=)
