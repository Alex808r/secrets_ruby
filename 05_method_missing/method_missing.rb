# frozen_string_literal: true

class Animal
  def method_missing(method, *_args)
    puts "No method #{method}"
  end
end

animal = Animal.new

animal.name
