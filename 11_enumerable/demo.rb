# frozen_string_literal: true

require_relative 'inventory'

sword = SimpleRPG::Item.new 'Sting', 10_000, '3rd Feb 2020'

ring = SimpleRPG::Item.new 'The One Ring', 100_000_000, '1rd Mar 2020'

armor = SimpleRPG::Item.new 'Mithril Armor', 100_000, '14rd Apr 2020'

lembas = SimpleRPG::Item.new 'Lembas', 10, '15th May 2020'

rope = SimpleRPG::Item.new 'Rope', 1, '16th May 2020'

# puts sword > ring
# puts armor.between?(sword, ring)

inventory = SimpleRPG::Inventory.new('Bilbo Baggins', sword, ring, armor)

inventory << [lembas, rope]
# puts inventory.inspect
# puts inventory.to_a

inventory.sort.each do |item|
  puts item
end

puts inventory[0]
