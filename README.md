### 1
```
ibr -r ./classes.rb 

a = Animal.new

a.class.class.superclass
=> Module

klass = Animal
klass.new

puts Animal::TEST
puts ::TEST
```

### 2
```
Singleton class

class Animal
  # singleton method
  self.find(args)
  end
end

Animal.singleton_class
Animal.singleton_class.instance_methods
Animal.singleton_class.instance_methods.grep 'fi'
Animal.instance_methods

class Cat < Animal
end

# Cat.find('serious')
```

### 3
```
Proc lambda block

my_proc = Proc.new {|msg| puts msg }
my_proc = proc {|msg| puts msg }

my_proc.call('hi')
my_proc.call('Hello')

my_lambda = lambda {|msg| puts msg}
my_lambda.call('It is lambda')

second_lambda = ->(msg) {puts msg} # stabby lambda | thin arrow
second_lambda.call('Second lambda')
```

### 4

```
Идиомы для производительного кода Ruby

https://github.com/evanphx/benchmark-ips

Репозитарий с исходными примерами: https://github.com/fastruby/fast-ruby.

Fasterer: https://github.com/DamirSvrtan/fasterer

Rubocop-performance: https://docs.rubocop.org/rubocop-performance/

rubocop --require rubocop-performance
```

### 5
```
Method mssing

def method_missing(method, *args, &block)
  method - symbol
  *args = Array argumemnts
end

def respond_to_missing?(method, include_private = false)
  method - symbol
end
```

### 6
```
Define method

supports :update, destroy

def supports(*methods)
  methods.each do |method|
    define_method(method) do |params = {}|
      self.class.send(method, params.merge(id: self.id))
    end
  end
end

define_method(method) do |params = {}| == def create(params)
    
def create(params)
  self.new post(params)
end
```

### 7
```
eval
instance_exec
```

### 8
```
refinements
monkey patching

module ExternalLib
  module Utils
    refine String do
      def snakecase
        some code
      end
    end
  end
end


module ExternalLib
  class Main
    using ExternalLib::Utils
  end
end
```

### 9
```
public protected pricate

current method private

class Animal
  private def maximun_foody
  end
end

grop methods private

class Animal
  
  private 

  def maximun_foody
  end

  def minimun_foody
  end

  attr_reader :secret
end

private
private method call only self
cat.can_eat_more?(other_cat) - dont work 
but
cat.send(:private_method)


protected 
private method call not only self
cat.search_url_for(other_cat) - it is work
```

### 10
```
'test'.bytes

XOR
input     0 1 1 0
secret    1 1 0 1 
crypted   1 0 1 1 
secret    1 1 0 1 
output    0 1 1 0
```

### 11
```
Enumerable, Comparable, mixins

def each
  @items.each { |item| yield(item) }
end

or

def each(&)
  @items.each(&)
end

def <=>(other)
  value <=> other.value
end
```

### 12
```
Forwardable

def_delegators :@items, :[], :each, :last

method missing

MEHTOD_REGEXP = /Ado_(get|post|put)\z/
  
def respond_to_missing(method, _include_all)
  return true if MEHTOD_REGEXP.match>(method.to_s)

  super
end

def method_missing(method, *_agrs)
  if method.to_s =~ MEHTOD_REGEXP
    send Regexp.last_match(1), prepare(@path),@params
  else
    super  
  end
end
```

### 13
```
Ruby Thread

gem install kramdown


thr = Thread.new { puts "Whats the big deal" }
thr.join #=> "Whats the big deal"

Thread.current[:name] = "thread name"
```

### 14
```
Enumerator::Lazy

to_a(force) or first
puts (1..1000).lazy.map { |i| puts(i);  i ** 2 }.
select(&:even?).take(3).to_a.inspect
# 1
# 2
# 3
# 4
# 5
# 6
# [4, 16, 36]

# without to_a or first
return Enumerator::Lazy object   
#<Enumerator::Lazy: #<Enumerator::Lazy: #<Enumerator::Lazy: #<Enumerator::Lazy: 1..1000>:map>:select>:take(3)>

# eager
def calc 
  (1..1000).lazy.map { |i| puts(i);  i ** 2 }.
  select(&:even?).take(10).eager
end

calc.select{|i| (i % 3).zero?}.force.inspect
```

### 15
```
Pattern matching
```
