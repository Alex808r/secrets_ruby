# frozen_string_literal: true

# my_proc = Proc.new {|msg| puts msg }
# my_proc = proc {|msg| puts msg }

# my_proc.call('hi')
# my_proc.call('Hello')

# my_lambda = lambda {|msg| puts msg}
# my_lambda.call('It is lambda')

# second_lambda = ->(msg) {puts msg} # stabby lambda | thin arrow
# second_lambda.call('Second lambda')

# Отличия
# 1.встречая return:
# proc выходит из себя и из метода
# lambda выходит из себя

# 2. лишний аргумент
#  proc опускает лишний аргументы
#  lambda следит за количеством аргументов

def my_method(callable)
  result = callable.call('Hello world', 1)

  puts "The result is #{result}"
  puts "I have called #{callable.inspect}"
end

my_lmb = proc do |msg|
  puts msg
  puts msg
  return msg
end

# my_lmb = proc do |msg| puts msg
#   puts msg
#   return msg
# end

my_method(my_lmb)
