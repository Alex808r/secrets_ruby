# frozen_string_literal: true

def my_method
  yield('Hello')
end

my_method do |msg|
  puts msg
end

def method_two
  yield('Hello method two')
end

method_two do |msg|
  puts msg
end

def foo
  yield('Bar')
end

lmb = ->(msg) { puts msg }
# &lmb символ & преобразует lambda в блок

# foo(lmb) # ошибка, тк метод не ждет аргументы
foo(&lmb) # ошибки нет, тк ламбда может переходит в блок а блок в лямбду

# Блок можно передать только один, процедур или лямбд много
def baz(msg, callable1, callable2)
  callable1.call('hello')
  callable2.call(msg)
end

lb1 = ->(m) { puts m }
lb2 = ->(m) { puts m.inspect }

baz('my message', lb1, lb2)
