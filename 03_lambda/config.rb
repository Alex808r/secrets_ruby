# frozen_string_literal: true

require_relative 'zipper/zipper'
require_relative 'zipper/processor'

Zipper.config do |config|
  config.extention = '.txt'
  config.processing = ->(content) { puts content.upcase }
  # config.processing = -> (content) {puts content}
end
