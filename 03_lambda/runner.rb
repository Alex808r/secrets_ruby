# frozen_string_literal: true

require_relative 'config'

Zipper::Processor.open_and_process_zip('./arch.zip')

# module Zip
#   module Processor
#     # class << self

#     def open_and_process_zip(args)
#       puts args
#     end
#   end
#   # end
# end

# Zip::Processor.open_and_process_zip('12345')

# module Foo
#   extend Zip::Processor
# end

# Foo.open_and_process_zip('12345')
