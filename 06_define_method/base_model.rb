# frozen_string_literal: true

require_relative 'request'

class BaseModel
  extend Request

  def initialize(params)
    params.each do |k, v|
      instance_variable_set "@#{k}", v
    end
  end

  class << self
    def supports(*methods)
      methods.each do |method|
        define_method(method) do |params = {}|
          self.class.send(method, params.merge(id: id))
        end
      end
    end

    def create(params)
      new post(params)
    end

    def update(params)
      new patch(params.delete(:id), params)
    end

    def destroy(params)
      delete(params[:id])
    end
  end
end
