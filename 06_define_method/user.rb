# frozen_string_literal: true

class User < BaseModel
  attr_accessor :id, :name, :surname

  supports :destroy
end
