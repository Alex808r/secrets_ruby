# frozen_string_literal: true

require_relative 'main'

post = BlogPost.create(title: 'My Post', body: 'Sample')

user = User.create(name: 'John', surname: 'Doe')

puts user.inspect
puts post.inspect

puts user.destroy
updated_post = post.update(title: 'Updated title', body: 'new body')
puts updated_post

# updated_post = BlogPost.update(id: post.id, title: 'Updated title', body: 'new body')
# result = User.destroy(id: user.id)

# puts updated_post.inspect
# puts result.inspect


# ruby runner.rb
# =>
# HTTP POST: {:title=>"My Post", :body=>"Sample"}
# HTTP POST: {:name=>"John", :surname=>"Doe"}
# #<User:0x0000000104a991d8 @name="John", @surname="Doe", @id=80336>
# #<BlogPost:0x0000000104a99890 @title="My Post", @body="Sample", @id=20556>
# HTTP DELETE: 80336
# {:id=>80336, :deleted=>true}
# HTTP PATCH: 20556 => {:title=>"Updated title", :body=>"new body"}
# #<BlogPost:0x0000000104a93ee0>

