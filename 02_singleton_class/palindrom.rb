# frozen_string_literal: true

# class String
#   def palindrome?
#     self.gsub(/\W/,'').downcase.reverse == self.gsub(/\W/,'').downcase
#   end
# end

str = get.strip
# Madam, I Am Adam

# Singleton method to String
def str.palindrome?
  gsub(/\W/, '').downcase.reverse == gsub(/\W/, '').downcase
end

puts str.singleton_class.instance_methods # => ...palindrome?

temp_str = srt.gsub(/\W/, '').downcase

puts 'Yes' if temp_str.reverse == temp_str
