# frozen_string_literal: true

# rubocop:disable Naming/VariableNumber

require 'selenium-webdriver'
require 'cgi'

class Animal < Object
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  # Singleton method
  # def Animal.find
  def self.find(term = '')
    driver = Selenium::WebDriver.for :firefox
    full_term = CGI.escape "#{term} #{name.downcase}"
    driver.get "https://ddg.gg/?q=#{full_term}"
  end

  def self.select; end

  class << self
    def find_2; end

    def find_3; end
  end
end

class Cat < Animal
end

# Cat.find('serious')

pp Animal.singleton_class
pp Animal.singleton_class.instance_methods
pp Animal.singleton_class.instance_methods.grep 'fi'
pp Animal.instance_methods

# rubocop:enable Naming/VariableNumber
