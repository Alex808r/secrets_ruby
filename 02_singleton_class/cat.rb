# frozen_string_literal: true

# rubocop:disable Naming/VariableNumber
module Foo
  def foo_1; end
end

module Bar
  def bar_1; end
end

class Cat < Animal
  extend Bar
  class << self
    include Foo
    attr_accessor :avg_hight
  end
end

Cat.avg_hight = 10
Cat.avg_hight
Cat.foo_1
Cat.bar_1
# rubocop:enable Naming/VariableNumber
