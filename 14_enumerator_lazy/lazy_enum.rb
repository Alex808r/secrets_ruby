# frozen_string_literal: true

# rubocop:disable all
puts (1..1000).map { |i| puts(i);  i ** 2 }.
select(&:even?).first(3).inspect
# 1
# ...
# 996
# 997
# 998
# 999
# 1000
# [4, 16, 36]


puts (1..1000).lazy.map { |i| puts(i);  i ** 2 }.
select(&:even?).first(3).inspect
# 1
# 2
# 3
# 4
# 5
# 6
# [4, 16, 36]

puts (1..1000).lazy.map { |i| puts(i);  i ** 2 }.
select(&:even?).take(3).inspect
#<Enumerator::Lazy: #<Enumerator::Lazy: #<Enumerator::Lazy: #<Enumerator::Lazy: 1..1000>:map>:select>:take(3)>
# The real enumeration is performed when any non-redefined Enumerable method is called, 
# like Enumerable#first or Enumerable#to_a (the latter is aliased as force for more semantic code):


#to_a (force) or first
puts (1..1000).lazy.map { |i| puts(i);  i ** 2 }.
select(&:even?).take(3).to_a.inspect
# 1
# 2
# 3
# 4
# 5
# 6
# [4, 16, 36]


puts (1..1000).lazy.map { |i| puts(i);  i ** 2 }.
select(&:even?).take(10).
select{|i| (i % 3).zero?}.force.inspect

# eager => return []
def calc 
  (1..1000).lazy.map { |i| puts(i);  i ** 2 }.
  select(&:even?).take(10).eager
end

calc.select{|i| (i % 3).zero?}.force.inspect

# rubocop:enable all