# frozen_string_literal: true

# File.open('alice_in_wonderland.txt') do |f|
#   puts f.each_line.with_index.
#     select {|line, i| puts(i) if (i %500).zero?; line.match(/Majesty/i)}.
#     first(3)
# end

File.open('alice_in_wonderland.txt') do |f|
  puts f.each_line.with_index.lazy
        .select { |line, i|
         puts(i) if (i % 500).zero?
         line.match(/Majesty/i)
       }
        .first(3)
end
