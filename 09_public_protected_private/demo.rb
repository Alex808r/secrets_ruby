# frozen_string_literal: true

require 'active_support/inflector/transliterate'
require 'cgi'
require 'digest'

class Animal
  include ActiveSupport::Inflector
  attr_reader :age

  def initialize(name, age)
    @name = name
    @age = age
    @secret = Digest::MD5.hexdigest "#{name} #{age}"
  end

  attr_reader :name

  private

  def minimun_foody
    # some code
  end

  def maximun_foody
    age > 2 ? 350 : 250
  end

  attr_reader :secret

  protected

  def to_params
    parameterize name
  end
end

class Cat < Animal
  def introduce
    puts "I am #{name} and I'm #{age}"
  end

  def say_maximum_foody
    puts "I cat eat only #{maximun_foody}g"
  end

  def can_eat_more?(other_cat)
    maximun_foody > other_cat.maximun_foody # other_cat NoMethodError maximun_foody
  end

  def url_for
    "http://example.com/cats/#{to_params}"
  end

  def search_url_for(other_cat)
    params = CGI.escape("#{to_params}, #{other_cat.to_params}")
    "http://example.com/cats?q=#{params}"
  end

  def reveal_secret
    secret
  end
end

cat = Cat.new('Spot', 1)
other_cat = Cat.new('Mr.buttons', 5)

# ============== #
# public
puts cat.name, cat.age
other_cat.introduce

# puts cat.public_methods
puts cat.public_methods.grep(/name/)

# ============== #
# private

# cat.maximun_foody # `<main>': private method `maximun_foody' called for
cat.say_maximum_foody
# puts cat.private_methods

# private can call only self!!!
# puts cat.can_eat_more?(other_cat) # `<main>': private method `maximun_foody' called for @name="Mr.buttons"

# ============== #
# protected
# cat.to_params protected method `to_params' called for #<Cat:0x00000001053feb10 @name="Spot
puts cat.url_for
puts cat.search_url_for(other_cat)
puts cat.reveal_secret
puts cat.send(:secret)
