# frozen_string_literal: true

require 'benchmark/ips'

def slow
  yield
end

def slow2
  yield
end

def slow3(&block); end

def fast
  yield
end

Benchmark.ips do |x|
  x.report('block.call') { slow { 1 + 1 } }
  x.report('block + yield') { slow2 { 1 + 1 } }
  x.report('unused block') { slow3 { 1 + 1 } }
  x.report('yield') { fast { 1 + 1 } }
  x.compare!
end

# unused block: 14532066.9 i/s
# yield: 13586623.9 i/s - 1.07x  slower
# block + yield: 11625019.1 i/s - 1.25x  slower
# block.call: 11207958.8 i/s - 1.30x  slower
