# frozen_string_literal: true

require 'benchmark/ips'
require 'date'

BEGIN_OF_JULY = Date.new(2015, 7, 1)
END_OF_JULY = Date.new(2015, 7, 31)
DAY_IN_JULY = Date.new(2015, 7, 15)

Benchmark.ips do |x|
  x.report('range#cover?') { (BEGIN_OF_JULY..END_OF_JULY).cover?(DAY_IN_JULY) }
  x.report('range#include?') { (BEGIN_OF_JULY..END_OF_JULY).cover?(DAY_IN_JULY) }
  x.report('range#member?') { (BEGIN_OF_JULY..END_OF_JULY).cover?(DAY_IN_JULY) }
  x.report('plain compare?') { BEGIN_OF_JULY < DAY_IN_JULY && DAY_IN_JULY < END_OF_JULY }

  x.compare!
end

# plain compare?:  6500002.9 i/s
# range#cover?:  5107187.9 i/s - 1.27x  slower
# range#member?:   235184.2 i/s - 27.64x  slower
# range#include?:   230948.6 i/s - 28.14x  slower
