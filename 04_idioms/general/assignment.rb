# frozen_string_literal: true

require 'benchmark/ips'

def fast
  _a = 1
  _b = 2
  _c = 3
end

def slow
  _a = 1
  _b = 2
  _c = 3
end

Benchmark.ips do |x|
  x.report('Parallel Assignment') { fast }
  x.report('Siquential Assignment') { slow }
  x.compare!
end

# Parallel Assignment: 12411025.9 i/s - 1.10x  slower
