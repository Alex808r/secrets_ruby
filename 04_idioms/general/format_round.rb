# frozen_string_literal: true

require 'benchmark/ips'

NUM = 1.231254

def avg
  format('%.2f', NUM)
end

def slow
  format('%.2f', NUM)
end

def fast
  NUM.round(2).to_s
end

Benchmark.ips do |x|
  x.report('String#%') { slow }
  x.report('Kernel#Format') { avg }
  x.report('Float#Round') { fast }
  x.compare!
end

# Kernel#Format:  2712079.5 i/s
# String#%:  2571029.1 i/s - 1.05x  slower
# Float#Round:  2013358.7 i/s - 1.35x  slower
