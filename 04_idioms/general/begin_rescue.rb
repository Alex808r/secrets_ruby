# frozen_string_literal: true

require 'benchmark/ips'

def slow
  written
rescue StandardError
  'slow ruby'
end

def fast
  if respond_to?(:written)
    written
  else
    'fast ruby'
  end
end

Benchmark.ips do |x|
  x.report('begin rescue') { slow }
  x.report('respond_to?') { fast }
  x.compare!
end

# begin rescue:  1548205.0 i/s - 6.18x  slower
