# frozen_string_literal: true

require 'benchmark/ips'
require 'ostruct'

# rubocop:disable Style/OpenStructUse
HASH = { field1: 1, field2: 2 }.freeze
OPENSTRUCT = OpenStruct.new(field1: 1, field2: 2)

def fast
  [HASH[:field1], HASH[:field2]]
end

def slow
  [OPENSTRUCT.field1, OPENSTRUCT.field2]
end

Benchmark.ips do |x|
  x.report('Hash') { fast }
  x.report('OpenStruct') { slow }
  x.compare!
end

# rubocop:enable Style/OpenStructUse

#  Hash:  7536465.1 i/s
#  OpenStruct:  4801548.1 i/s - 1.57x  slower
