# frozen_string_literal: true

require 'benchmark/ips'
NUMBER = 100_000_000

def fast
  index = 0
  loop do
    break if index > NUMBER

    index += 1
  end
end

def slow
  index = 0
  loop do
    break if index > NUMBER

    index += 1
  end
end

Benchmark.ips do |x|
  x.report('While') { fast }
  x.report('Loop') { slow }
  x.compare!
end

# While:        0.5 i/s
# Loop:        0.2 i/s - 2.20x  slower
