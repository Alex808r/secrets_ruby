# frozen_string_literal: true

require 'benchmark/ips'

class User
  attr_accessor :first_name, :last_name
end

def slow
  user = User.new
  user.last_name = 'Foo'
  user.last_name
end

def fast
  user = User.new
  user.first_name = 'Foo'
  user.first_name
end

Benchmark.ips do |x|
  x.report('getter_and_setter') { slow }
  x.report('attr_acessor') { fast }
  x.compare!
end

# getter_and_setter:  6038412.8 i/s - 1.07x  slower
