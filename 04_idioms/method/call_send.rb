# frozen_string_literal: true

require 'benchmark/ips'

class MethodCall
  def method; end

  def method_missing(_method, *_args)
    method
  end
end

def fastest
  method = MethodCall.new
  method.method
end

def slow
  method = MethodCall.new
  method.send(:method)
end

def slowest
  method = MethodCall.new
  method.not_exist
end

Benchmark.ips do |x|
  x.report('call') { fastest }
  x.report('send') { slow }
  x.report('method_missing') { slowest }
  x.compare!
end

# call:  7972199.1 i/s
# send:  6526555.9 i/s - 1.22x  slower
# method_missing:  5137258.2 i/s - 1.55x  slower
