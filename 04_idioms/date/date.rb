# frozen_string_literal: true

require 'benchmark/ips'
require 'date'

STRING = '2018-03-21'

def fast
  Date.iso8601(STRING)
end

def slow
  Date.parse(STRING)
end

Benchmark.ips do |x|
  x.report('Date.iso8601') { fast }
  x.report('Date.parse') { slow }
  x.compare!
end

# Date.iso8601:  1035363.6 i/s
# Date.parse:   543516.9 i/s - 1.90x  slowerz
