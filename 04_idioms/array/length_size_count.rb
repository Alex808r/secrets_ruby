# frozen_string_literal: true

require 'benchmark/ips'

ARRAY = [*1..100]

Benchmark.ips do |x|
  x.report('Array#length') { ARRAY.length }
  x.report('Array#size') { ARRAY.size }
  x.report('Array#count') { ARRAY.count }
  x.compare!
end

# Array#size: 22539443.7 i/s
# Array#length: 22517248.4 i/s - same-ish: difference falls within error
# Array#count: 17047758.9 i/s - 1.32x  slower
