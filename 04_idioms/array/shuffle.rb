# frozen_string_literal: true

require 'benchmark/ips'

ARRAY = [*1..100]

def slow
  ARRAY.sample
end

def fast
  ARRAY.sample
end

Benchmark.ips do |x|
  x.report('Array#shuffle.first') { slow }
  x.report('Array#sample') { fast }
  x.report('rand') do
    random = rand 0...ARRAY.size
    ARRAY[random]
  end
  x.compare!
end

# Array#sample: 11114626.1 i/s
# rand:  6696702.1 i/s - 1.66x  slower
# Array#shuffle.first:   615277.0 i/s - 18.06x  slower
