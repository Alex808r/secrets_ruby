require 'benchmark/ips'

ARRAY = [*1..100]

def fast
  ARRAY.take(3)
end

def slow
  ARRAY[..2]
end

Benchmark.ips do |x|
  x.report('Array#take') { fast }
  x.report('Array#p[]') { slow }
  x.compare!
end

# Array#take: 11111907.3 i/s
# Array#p[]: 10733041.4 i/s - 1.04x  slower