# frozen_string_literal: true

require 'benchmark/ips'

Benchmark.ips do |x|
  x.report('Array#unshift') do
    array = []
    100_000.times { |number| array.unshift(number) }
  end

  x.report('Array#insert') do
    array = []
    100_000.times { |number| array.insert(0, number) }
  end

  x.report('Array#push') do
    array = []
    100_000.times { |number| array.push(number) }
  end

  x.compare!
end

# Array#push:      265.0 i/s
# Array#unshift:      253.3 i/s - 1.05x  slower
# Array#insert:        1.3 i/s - 208.09x  slower
