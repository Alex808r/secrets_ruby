# frozen_string_literal: true

require 'benchmark/ips'

ARRAY = (1..100).to_a.freeze

def fast
  ARRAY[0]
end

def slow
  ARRAY.first
end

Benchmark.ips do |x|
  x.report('Array#[]') { fast }
  x.report('Array#first') { slow }
  x.compare!
end

# Array#[]: 15270294.2 i/s
# Array#first: 13111986.9 i/s - 1.16x  slower
