# frozen_string_literal: true

require 'benchmark/ips'

ARRAY = [*1..100]

def slow
  ARRAY.select { |x| x.eql?(15) }.first
end

def fast
  ARRAY.detect { |x| x.eql?(15) }
end

Benchmark.ips do |x|
  x.report('select first') { slow }
  x.report('detected') { fast }
  x.compare!
end

# detected:  1137551.7 i/s
# select first:   268021.6 i/s - 4.24x  slower
