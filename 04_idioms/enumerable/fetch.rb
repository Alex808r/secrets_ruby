# frozen_string_literal: true

require 'benchmark/ips'

HASH_WITH_SYMBOL = { fast: 'ruby' }
HASH_WITH_STRING = { 'fast' => 'ruby' }

def fastest
  HASH_WITH_SYMBOL[:fast]
end

def faster
  HASH_WITH_SYMBOL.fetch(:fast)
end

def fast
  HASH_WITH_STRING['fast']
end

def slow
  HASH_WITH_STRING.fetch('fast')
end

Benchmark.ips do |x|
  x.report('Hash#[] symdol') { fastest }
  x.report('Hash#fetch symbol') { faster }
  x.report('Hash#[] string') { fast }
  x.report('Hash#fetch string') { slow }
  x.compare!
end

# Hash#[] string: 13697440.7 i/s
# Hash#[] symdol: 11538416.9 i/s - 1.19x  slower
# Hash#fetch symbol: 10072179.9 i/s - 1.36x  slower
# Hash#fetch string:  9104295.5 i/s - 1.50x  slower
