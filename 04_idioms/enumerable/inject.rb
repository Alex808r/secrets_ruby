# frozen_string_literal: true

require 'rubygems'
require 'benchmark/ips'

ARRAY = (1..1000).to_a

def faster
  ARRAY.inject(:+)
end

def fast
  ARRAY.inject(&:+)
end

def slow
  ARRAY.inject { |a, i| a + i }
end

Benchmark.ips do |x|
  x.report('inject symbol') { faster }
  x.report('inject t_proc') { fast }
  x.report('inject block') { slow }
  x.compare!
end

# inject symbol:   924212.8 i/s
# inject t_proc:    28351.2 i/s - 32.60x  slower
# inject block:    28307.6 i/s - 32.65x  slower
