# frozen_string_literal: true

require 'benchmark/ips'

ARRAY = [*1..100]

def fast
  index = 0
  while index < ARRAY.size
    ARRAY[index] + index
    index += 1
  end
  ARRAY
end

def slow
  ARRAY.each_with_index do |number, index|
    number + index
  end
end

Benchmark.ips do |x|
  x.report('while loop') { fast }
  x.report('each_with_index') { slow }
  x.compare!
end

# each_with_index:   261383.8 i/s
# while loop:   230226.9 i/s - 1.14x  slower
