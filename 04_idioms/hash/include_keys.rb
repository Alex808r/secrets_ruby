# frozen_string_literal: true

require 'benchmark/ips'

HASH = Hash[*('a'..'zzz').to_a.shuffle]
KEY = 'zz'

def key_fast
  HASH.key?(KEY)
end

def key_slow
  HASH.keys.include?(KEY)
end

Benchmark.ips do |x|
  x.report('Hash#keys.include?') { key_slow }
  x.report('Hash#keys?') { key_fast }
  x.compare!
end

# Hash#keys?: 11366486.2 i/s
# Hash#keys.include?:    15511.3 i/s - 732.79x  slower
