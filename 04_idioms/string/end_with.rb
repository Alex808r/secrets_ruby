# frozen_string_literal: true

require 'benchmark/ips'

SLUG = 'some_kind_of_root_url'

def slower
  SLUG =~ /_(path|url)$/
end

def slow
  SLUG.match?(/_(path|url)$/)
end

def fast
  SLUG.end_with?('_path', '_url')
end

Benchmark.ips do |x|
  x.report('String#=~') { slower }
  x.report('String#match?') { slow }
  x.report('String#end_with?') { fast }
  x.compare!
end

# String#end_with?:  7683118.4 i/s
# String#match?:  5391531.4 i/s - 1.43x  slower
# String#=~:  2570871.5 i/s - 2.99x  slower
