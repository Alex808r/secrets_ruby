# frozen_string_literal: true

require 'benchmark/ips'

PASSAGE = <<~LIPSUM
  Lorem   ipsum dolor sit amet, consectetur adipiscing elit.#{' '}
  Aliquam lobortis   erat eget risus congue, eu egestas libero semper.#{' '}
  Donec elementum   leo non metus consequat, vulputate dapibus eros vestibulum.#{' '}
  Vestibulum ornare   sem sapien, sed iaculis neque posuere rutrum.#{' '}
  Proin eu consectetur ex.#{' '}
  Nullam convallis urna lacus, et lobortis lorem malesuada tincidunt.#{' '}
  In bibendum lacus vel pulvinar  volutpat.#{' '}
  Donec ac dapibus   eros.
LIPSUM

raise unless PASSAGE.squeeze(' ') == PASSAGE.squeeze(' ')

def slow
  PASSAGE.gsub(/ \s+/, ' ')
end

def fast
  PASSAGE.squeeze(' ')
end

Benchmark.ips do |x|
  x.report('String#gsub/regexp+/') { slow }
  x.report('String#squeeze') { fast }

  x.compare!
end

# String#squeeze:  2519065.7 i/s
# String#gsub/regexp+/:    44081.8 i/s - 57.15x  slower
