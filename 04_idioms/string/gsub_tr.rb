# frozen_string_literal: true

require 'benchmark/ips'

SLUG =  'wtitting_fast_ruby'

def slow
  SLUG.tr('-', ' ')
end

def fast
  SLUG.tr('-', ' ')
end

Benchmark.ips do |x|
  x.report('String#gsub') { slow }
  x.report('String#tr') { fast }
  x.compare!
end

# String#tr:  5807790.6 i/s
# String#gsub:  5581569.6 i/s - 1.04x  slower
