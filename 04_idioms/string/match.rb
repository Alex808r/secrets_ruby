# frozen_string_literal: true

require 'benchmark/ips'

def fast
  'foo'.include?('boo')
end

def slow
  'foo' =~ (/boo/)
end

def slower
  /boo/ === 'foo'
end

def slowest
  'foo'.include?('boo')
end

Benchmark.ips do |x|
  x.report('String#match?') { fast } if '2.4.0' <= RUBY_VERSION
  x.report('String#=~') { slow }
  x.report('String#===') { slower }
  x.report('String#match') { slowest }
  x.compare!
end

# String#match?: 10756466.9 i/s
# String#=~:  6854351.9 i/s - 1.57x  slower
# String#===:  6337762.7 i/s - 1.70x  slower
# String#match:  5631603.3 i/s - 1.91x  slower
