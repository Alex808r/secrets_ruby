# frozen_string_literal: true

require 'benchmark/ips'

SLUG = 'YourSubClassType'

def slow
  SLUG.delete_suffix('Type')
end

def fast
  SLUG.chomp('Type')
end

def faster
  SLUG.delete_suffix('Type')
end

Benchmark.ips do |x|
  x.report('String#sub') { slow }
  x.report('String#chomp') { fast }
  x.report('String#delete_suffix') { faster }
  x.compare!
end

# String#delete_suffix:  7834723.2 i/s
# String#chomp:  7471852.1 i/s - 1.05x  slower
# String#sub:  2344644.0 i/s - 3.34x  slower
