# frozen_string_literal: true

require 'benchmark/ips'

URL = 'http://www.asdfasdfasdfasdfasdfadsgkdgjdkfsknbn.com'

def slow
  URL.gsub('http://', 'https://')
end

def fast
  URL.sub('http://', 'https://')
end

def fastest
  str = URL.dup
  str['http://'] = 'https://'
  str
end

Benchmark.ips do |x|
  x.report('String#gsub') { slow }
  x.report('String#sub') { fast }
  x.report('String#dup ["string"]') { fastest }
  x.compare!
end

# String#dup ["string"]:  2761847.4 i/s
# String#sub:  2105195.2 i/s - 1.31x  slower
# String#gsub:  1926834.7 i/s - 1.43x  slower
