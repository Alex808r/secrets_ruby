# frozen_string_literal: true

require 'benchmark/ips'

def without_freeze
  'To freeze or not to freeze'
end

def with_freeze
  'To freeze or not to freeze'
end

Benchmark.ips do |x|
  x.report('without_freeze') { without_freeze }
  x.report('with_freeze') { with_freeze }
  x.compare!
end

# without_freeze: 17078692.3 i/s
# with_freeze: 16917537.6 i/s - same-ish: difference falls within error
