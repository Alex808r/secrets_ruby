# frozen_string_literal: true

require 'benchmark/ips'

def slow_plus
  'foobar'
end

def slow_concat
  'foo'.concat('bar')
end

def slow_append
  'foo' << 'bar'
end

def fast
  'foo' 'bar'
end

def fast_interpolation
  'foobar'
end

Benchmark.ips do |x|
  x.report('String#+') { slow_plus }
  x.report('String#concat') { slow_concat }
  x.report('String#append') { slow_append }
  x.report('"foo" "bar"') { fast }
  x.report(%("#{foo}#{bar}")) { fast_interpolation }
  x.compare!
end

# "#{foo}#{bar}": 13417967.1 i/s
# "foo" "bar": 13302770.3 i/s - same-ish: difference falls within error
# String#append:  8509514.9 i/s - 1.58x  slower
# String#concat:  8023325.6 i/s - 1.67x  slower
#   String#+:  7944284.1 i/s - 1.69x  slower
