# frozen_string_literal: true

ages = [28, 30, 35, 42, 50]
sherlock_age = 30

case ages
in watson_age, ^sherlock_age, *other_ages
  puts watson_age
  puts sherlock_age
end
