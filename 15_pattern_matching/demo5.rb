# frozen_string_literal: true

ages = [28, 30, 45]
case ages
in [Integer => age1, Integer => age2, *]
  puts age1, age2
end

users = [:ok,
         [
           {
             name: 'Sherlock',
             surname: 'Holems',
             extra: {
               age: '30',
               hobby: 'violin'
             }
           },

           {
             name: 'John',
             surname: 'Watson',
             extra: {
               age: '28',
               hobby: 'writing'
             }
           }
         ]]

case users
in :ok, [{extra: {age: sherlock_age}}, *]
  puts sherlock_age
in :error, error
  puts error
else
  'no match'
end
