# frozen_string_literal: true

data = {
  name: 'Sherlock',
  surname: 'Holmes',
  extra: {
    age: '30',
    hobby: 'violin'
  }
}

case data
in name: _, surname:
  puts surname
end
