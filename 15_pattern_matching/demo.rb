# frozen_string_literal: true

data = {
  name: 'Sherlock',
  surname: 'Holmes',
  extra: {
    age: '30',
    hobby: 'violin'
  }
}

# case data
#   in {name:, extra: {age:}}
#   puts name
#   puts age
#   in extra: {univesity:}
#   puts univesity
# else
#   puts 'No match!'
# end

# case data
#   in {name: sherlock_name, extra: {age: sherlock_age}}
#   puts sherlock_name
#   puts sherlock_age
#   in extra: {univesity:}
#   puts univesity
# else
#   puts 'No match!'
# end

data => {extra: {age: sherlock_age}}
puts sherlock_age
