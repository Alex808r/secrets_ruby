# frozen_string_literal: true

ages = { sherlock: 30, watson: 28, lestrade: 40, hudson: 60 }
case ages
in { sherlock: Integer => sherlock_age }
  puts sherlock_age
end

ages = { sherlock: 30, watson: 28 }
case ages
in { sherlock:, **nil }
  puts sherlock
in { sherlock:, watson:, **nil }
  puts sherlock
  puts watson
end
