# frozen_string_literal: true

users = {
  required_name: 'John',
  users: [
    {
      name: 'Sherlock',
      surname: 'Holems',
      extra: {
        age: '30',
        hobby: 'violin'
      }
    },

    {
      name: 'John',
      surname: 'Watson',
      extra: {
        age: '28',
        hobby: 'writing'
      }
    }
  ]
}

case users
in required_name:, users: [*, {name: ^required_name, extra: {age:}}, *]
  puts required_name
  puts age
end
