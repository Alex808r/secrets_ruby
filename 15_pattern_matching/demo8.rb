# frozen_string_literal: true

users = [:ok,
         [
           {
             name: 'Sherlock',
             surname: 'Holems',
             extra: {
               age: '30',
               hobby: 'violin'
             }
           },

           {
             name: 'John',
             surname: 'Watson',
             extra: {
               age: '28',
               hobby: 'writing'
             }
           }
         ]]

name = 'Jhon'

case users
in :ok, [*, { name: ^name, extra: {age: } }, *]
  puts age
end
