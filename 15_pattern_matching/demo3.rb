# frozen_string_literal: true

ages = [28, 30]
case ages
in Integer, Integer
  puts 'Correct!'
else
  puts 'Incorrect!'
end
# => 'Correct!'

ages = [28, 30, 50]
case ages
in Integer, Integer
  puts 'Correct!'
else
  puts 'Incorrect!'
end
# => 'Incorrect!'

ages = [28, 30, 50]
case ages
in [Integer, Integer, *]
  puts 'Correct!'
else
  puts 'Incorrect!'
end
# => 'Correct!'

ages = { sherlock: 30, watson: 28, lestrade: 40 }
case ages
in {sherlock:, watson:, ** } # ** можно опустить
  puts 'Correct hash age!'
else
  puts 'Incorrect!'
end
# => Correct hash age!

ages = { sherlock: 30, watson: 28, lestrade: 40, hudson: 60 }
case ages
in { sherlock:, watson:, **other }
  puts "Other: #{other}"
else
  puts 'Incorrect!'
end
# => Other: {:lestrade=>40, :hudson=>60}

ages = [28, 30, 50, 60]
case ages
in [28, *other]
  puts "Other Array: #{other}"
end
# => Other Array: [30, 50, 60]
