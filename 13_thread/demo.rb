# frozen_string_literal: true

ts = (1..5).map do |i|
  Thread.new do
    Thread.current[:name] = "thread #{i}"
    sleep 1
    puts "done #{i}"
  end
end

puts 'starting...'
Thread.list.each { |t| puts t.inspect }
ts.each(&:join)
puts 'done with threads'

ts.each { |t| puts t[:name] }
