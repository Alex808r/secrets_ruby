# frozen_string_literal: true

ts = (1..5).map do |i|
  Thread.new do
    sleep 1
    puts "done #{i}"
    Thread.current.kill if i == 3
    rand(1..100)
  end
end

ts[0].kill

puts 'starting...'

results = ts.map(&:value)

puts 'done with threads'

puts results.inspect
