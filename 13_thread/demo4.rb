# frozen_string_literal: true

class Demo
  attr_reader :val

  def initialize
    @val = 0
  end

  def set(new_val)
    @val = new_val
  end
end

obj = Demo.new

ts = (1..10).map do |i|
  Thread.new do
    sleep rand(3)
    obj.set(i)
  end
end

ts.each(&:join)

puts obj.val
