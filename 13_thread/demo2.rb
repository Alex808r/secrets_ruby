# frozen_string_literal: true

ts = (1..5).map do |i|
  Thread.new do
    sleep 1
    puts "done #{i}"
    raise('error') if i == 3

    rand(1..100)
  end
end

puts 'starting...'

begin
  results = ts.map(&:value)
rescue StandardError => e
  puts 'EXCEPTION'
  puts e.inspect
  ts.each(&:join)
end

puts 'done with threads'

puts results.inspect
