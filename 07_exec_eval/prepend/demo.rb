# frozen_string_literal: true

module AnimalModule
  def speak
    puts "#{self.class.name} can't talk really ..."
  end
end

class Animal
  # include AnimalModule
  prepend AnimalModule

  def speak
    puts '...said animal'
  end
end

class Cat < Animal
  def speak
    puts 'Meow!'
  end
end

cat = Cat.new
cat.speak

puts 'ANCESTORS FOR CAT:'
puts Cat.ancestors

# ruby demo.rb
# =>
# Meow!
# ANCESTORS FOR CAT:
# Cat           !!!
# Animal        !!!
# AnimalModule
# Object
# Kernel
# BasicObject

# usees prepend
# ruby demo.rb
# =>
# Meow!
# ANCESTORS FOR CAT:
# Cat          !!!
# AnimalModule !!!
# Animal
# Object
# Kernel
# BasicObject
