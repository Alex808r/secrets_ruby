# frozen_string_literal: true

str = 'madam'

str.instance_exec do
  def palindrome?
    str_no_spaces = gsub(/\s/, '')
    str_no_spaces == str_no_spaces.reverse
  end
end

puts str.palindrome?
